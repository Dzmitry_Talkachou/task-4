package by.dzmitry.parserxml.enumeration;

/**
 * The Enum FormFactorType.
 */
public enum FormFactorType {

	/** The atx. */
	ATX("atx"),

	/** The mini atx. */
	MINI_ATX("mini-atx"),

	/** The micro atx. */
	MICRO_ATX("micro-atx"),

	/** The itx. */
	ITX("itx"),

	/** The mini itx. */
	MINI_ITX("mini-itx");

	/** The text. */
	private String text;

	/**
	 * Instantiates a new form factor type.
	 *
	 * @param text
	 *            the text
	 */
	FormFactorType(String text) {
		this.text = text;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * From string.
	 *
	 * @param text
	 *            the text
	 * @return the form factor type
	 */
	public static FormFactorType fromString(String text) {
		FormFactorType value = null;
		for (FormFactorType enumeration : FormFactorType.values()) {
			if (text.equalsIgnoreCase(enumeration.text)) {
				value = enumeration;
			}
		}
		return value;
	}
}