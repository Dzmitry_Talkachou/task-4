package by.dzmitry.parserxml.enumeration;

/**
 * The Enum FieldEnum.
 */
public enum FieldEnum {

	/** The device id. */
	// Device
	DEVICE_ID("device-id"),

	/** The name. */
	NAME("name"),

	/** The origin. */
	ORIGIN("origin"),

	/** The price. */
	PRICE("price"),

	/** The critical. */
	CRITICAL("critical"),

	/** The brand. */
	BRAND("brand"),

	/** The com port. */
	// Motherboard extends Device
	COM_PORT("com-port"),

	/** The lpt port. */
	LPT_PORT("ltp-port"),

	/** The usb port. */
	USB_PORT("usb-port"),

	/** The sata port. */
	SATA_PORT("sata-port"),

	/** The psi express slot. */
	PSI_EXPRESS_SLOT("psi-express-slot"),

	/** The form factor. */
	FORM_FACTOR("form-factor"),

	/** The cpu socket. */
	CPU_SOCKET("cpu-socket"),

	/** The air cooling. */
	// Videocard extends Device
	AIR_COOLING("air-cooling"),

	/** The dvi port. */
	DVI_PORT("dvi-port"),

	/** The hdmi port. */
	HDMI_PORT("hdmi-port"),

	/** The mini hdmi port. */
	MINI_HDMI_PORT("mini-hdmi-port"),

	/** The display port. */
	DISPLAY_PORT("display-port"),

	/** The mini display port. */
	MINI_DISPLAY_PORT("mini-displayPort"),

	/** The vga port. */
	VGA_PORT("vga-port"),

	/** The gpu clock. */
	GPU_CLOCK("gpu-clock"),

	/** The memory clock. */
	MEMORY_CLOCK("memory-clock");

	/** The text. */
	private String text;

	/**
	 * Instantiates a new field enum.
	 *
	 * @param text
	 *            the text
	 */
	FieldEnum(String text) {
		this.text = text;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * From string.
	 *
	 * @param text
	 *            the text
	 * @return the field enum
	 */
	public static FieldEnum fromString(String text) {
		FieldEnum value = null;
		for (FieldEnum enumeration : FieldEnum.values()) {
			if (text.equalsIgnoreCase(enumeration.text)) {
				value = enumeration;
			}
		}
		return value;
	}
}