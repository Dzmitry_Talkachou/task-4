package by.dzmitry.parserxml.enumeration;

/**
 * The Enum OriginType.
 */
public enum OriginType {

	/** The china. */
	CHINA("china"),
	/** The germany. */
	GERMANY("germany"),
	/** The usa. */
	USA("usa"),
	/** The taiwan. */
	TAIWAN("taiwan");

	/** The text. */
	private String text;

	/**
	 * Instantiates a new origin type.
	 *
	 * @param text
	 *            the text
	 */
	 OriginType(String text) {
		this.text = text;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * From string.
	 *
	 * @param text
	 *            the text
	 * @return the origin type
	 */
	public static OriginType fromString(String text) {
		OriginType value = null;
		for (OriginType enumeration : OriginType.values()) {
			if (text.equalsIgnoreCase(enumeration.text)) {
				value = enumeration;
			}
		}
		return value;
	}
}