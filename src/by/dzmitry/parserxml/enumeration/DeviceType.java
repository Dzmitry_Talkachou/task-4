package by.dzmitry.parserxml.enumeration;

/**
 * The Enum DeviceType.
 */
public enum DeviceType {

	/** The motherboard. */
	MOTHERBOARD("motherboard"),

	/** The videocard. */
	VIDEOCARD("videocard");

	/** The text. */
	private String text;

	/**
	 * Instantiates a new device type.
	 *
	 * @param text
	 *            the text
	 */
	DeviceType(String text) {
		this.text = text;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * From string.
	 *
	 * @param text
	 *            the text
	 * @return the device type
	 */
	public static DeviceType fromString(String text) {
		DeviceType value = null;
		for (DeviceType enumeration : DeviceType.values()) {
			if (text.equalsIgnoreCase(enumeration.text)) {
				value = enumeration;
			}

		}
		return value;
	}
}