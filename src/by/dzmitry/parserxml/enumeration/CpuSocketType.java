package by.dzmitry.parserxml.enumeration;

/**
 * The Enum CpuSocketType.
 */
public enum CpuSocketType {

	/** The AM2. */
	AM2("am2"),

	/** The AM2 plus. */
	AM2_PLUS("am2-plus"),

	/** The AM3. */
	AM3("am3"),

	/** The SOCKET-1156. */
	SOCKET_1156("socket-1156"),

	/** The SOCKET-1155. */
	SOCKET_1155("socket-1155"),

	/** The SOCKET-1150. */
	SOCKET_1150("socket-1150"),

	/** The SOCKET-1151. */
	SOCKET_1151("socket-1151");

	/** The text. */
	private String text;

	/**
	 * Instantiates a new cpu socket type.
	 *
	 * @param text
	 *            the text
	 */
	CpuSocketType(String text) {
		this.text = text;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * From string.
	 *
	 * @param text
	 *            the text
	 * @return the cpu socket type
	 */
	public static CpuSocketType fromString(String text) {
		CpuSocketType value = null;
		for (CpuSocketType enumeration : CpuSocketType.values()) {
			if (text.equalsIgnoreCase(enumeration.text)) {
				value = enumeration;
			}
		}
		return value;
	}
}