package by.dzmitry.parserxml.enumeration;

import by.dzmitry.parserxml.command.ActionCommand;
import by.dzmitry.parserxml.command.ParserCommand;

public enum CommandEnum {

	PARSER {
		{
			this.command = new ParserCommand();
		}
	};

	ActionCommand command;

	public ActionCommand getCurrentCommand() {

		return command;
	}
}