package by.dzmitry.parserxml.enumeration;

/**
 * The Enum BrandType.
 */
public enum BrandType {
	
	/** The asus. */
	ASUS("asus"), 
	
	/** The asrock. */
	ASROCK("asrock"),
	
	/** The gigabyte. */
	GIGABYTE("gigabyte"), 
	
	/** The palit. */
	PALIT("palit"),

	/** The sapphire. */
	SAPPHIRE("sapphire");

	/** The text. */
	private String text;

	/**
	 * Instantiates a new brand type.
	 *
	 * @param text the text
	 */
	BrandType(String text) {
		this.text = text;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * From string.
	 *
	 * @param text the text
	 * @return the brand type
	 */
	public static BrandType fromString(String text) {
		
		BrandType value = null;
		
		for (BrandType enumeration : BrandType.values()) {
			if (text.equalsIgnoreCase(enumeration.text)) {
				value = enumeration;
			}
		}
		return value;
	}
}