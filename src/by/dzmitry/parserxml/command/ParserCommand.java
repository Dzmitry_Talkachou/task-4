package by.dzmitry.parserxml.command;

import java.util.ArrayList;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.dzmitry.parserxml.exception.ParserXmlException;
import by.dzmitry.parserxml.logic.ParserLogic;
import by.dzmitry.parserxml.manager.ConfigurationManager;
import by.dzmitry.parserxml.manager.MessageManager;
import by.dzmitry.parserxml.parser.ParcerXml;
import by.dzmitry.parserxml.parser.ParsedItem;
import by.dzmitry.parserxml.parser.ParserFactory;
import by.dzmitry.parserxml.parser.ParserType;

public class ParserCommand implements ActionCommand {

	private static final Logger LOGGER = LogManager.getLogger(ParserCommand.class.getName());

	@Override
	public String execute(HttpServletRequest request) {

		Cookie[] cookies = request.getCookies();

		String pageSelectorValue = null;
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("page_selector")) {
					pageSelectorValue = cookie.getValue();
				}
			}
		}

		String page = null;
		String action = request.getParameter("parserType");
		String path = request.getServletContext().getRealPath("/");

		ParcerXml parser = ParserFactory.create(ParserType.fromString(action));
		try {
			ArrayList<ParsedItem> parsedItems = ParserLogic.parse(path, parser, pageSelectorValue);

			request.setAttribute("lst", parsedItems);

			request.setAttribute(action.toLowerCase(), "checked");

			request.getRequestDispatcher("path.page.index");

			page = ConfigurationManager.getProperty("path.page.product.list." + pageSelectorValue);

			return page;

		} catch (ParserXmlException e) {
			LOGGER.log(Level.ERROR, "Parser is not created." + e);
			request.setAttribute("parserError", action + MessageManager.getProperty("message.parsererror"));
		}
		return page;
	}
}