package by.dzmitry.parserxml.parser;

import java.util.ArrayList;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.dzmitry.parserxml.entity.Device;
import by.dzmitry.parserxml.entity.Motherboard;
import by.dzmitry.parserxml.entity.Videocard;
import by.dzmitry.parserxml.enumeration.BrandType;
import by.dzmitry.parserxml.enumeration.CpuSocketType;
import by.dzmitry.parserxml.enumeration.DeviceType;
import by.dzmitry.parserxml.enumeration.FieldEnum;
import by.dzmitry.parserxml.enumeration.FormFactorType;
import by.dzmitry.parserxml.enumeration.OriginType;
import by.dzmitry.parserxml.factory.DeviceFactory;

/**
 * The Class DeviceParserService.
 */
public class DeviceBuilder implements ParserBuilder {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(DeviceBuilder.class.getName());
	
	/** The device. */
	private ParsedItem device;
	private ArrayList<DeviceType> deviceType = new ArrayList<>();

	public DeviceBuilder add(DeviceType device) {
		deviceType.add(device);
		return this;
	}

	/** The devices. */
	private ArrayList<ParsedItem> devices = new ArrayList<>();

	@Override
	public ArrayList<ParsedItem> getAll() {
		ArrayList<ParsedItem> newDevices = this.devices;
		clear();
		return newDevices;
	}

	@Override
	public void clear() {

		this.device = null;
		deviceType = new ArrayList<>();
		this.devices = new ArrayList<>();
	}

	@Override
	public boolean checkForNewItem(String text) {
		boolean value = false;
		for (DeviceType type : deviceType) {
			if (text.equalsIgnoreCase(type.getText())) {
				value = true;
			}
		}
		return value;
	}

	@Override
	public boolean checkField(String text) {
		boolean value = false;
		for (FieldEnum fieldEnum : FieldEnum.values()) {
			if (text.equalsIgnoreCase(fieldEnum.getText())) {
				value = true;
			}
		}
		return value;
	}

	@Override
	public ArrayList<String> getFieldsName() {
		ArrayList<String> fieldsName = new ArrayList<>();
		for (FieldEnum fieldEnum : FieldEnum.values()) {
			fieldsName.add(fieldEnum.getText());
		}
		return fieldsName;
	}

	@Override
	public ArrayList<String> getParsedItemName() {
		ArrayList<String> getParsedItemName = new ArrayList<>();
		for (DeviceType type : deviceType) {
			getParsedItemName.add(type.getText());
		}
		return getParsedItemName;
	}

	@Override
	public boolean createItem(String text) {
		DeviceFactory devFactory = new DeviceFactory();
		boolean value = false;
		for (DeviceType type : deviceType) {
			if (text.equalsIgnoreCase(type.getText())) {
				this.device = devFactory.create(type);
				value = true;
			}
		}
		return value;
	}

	@Override
	public void completeItem() {
		devices.add(this.device);
	}

	@SuppressWarnings("incomplete-switch")
	@Override
	public boolean fillItem(String fieldName, String fieldValue) {

		boolean value = false;

		if (device instanceof Device) {

			switch (FieldEnum.fromString(fieldName)) {
			case DEVICE_ID:
				((Device) device).setDeviceId(fieldValue);
				break;
			case NAME:
				((Device) device).setName(fieldValue);
				break;
			case ORIGIN:
				((Device) device).setOrigin(OriginType.fromString(fieldValue));
				break;
			case PRICE:
				((Device) device).setPrice(Integer.valueOf(fieldValue));
				break;
			case CRITICAL:
				((Device) device).setCritical(Boolean.valueOf(fieldValue));
				break;
			case BRAND:
				((Device) device).setBrand(BrandType.fromString(fieldValue));
				break;
			}
		}
		if (device instanceof Motherboard) {

			value = true;

			switch (FieldEnum.fromString(fieldName)) {
			case COM_PORT:
				((Motherboard) device).setComPort(Boolean.valueOf(fieldValue));
				break;
			case LPT_PORT:
				((Motherboard) device).setLptPort(Boolean.valueOf(fieldValue));
				break;
			case USB_PORT:
				((Motherboard) device).setUsbPort(Integer.valueOf(fieldValue));
				break;
			case SATA_PORT:
				((Motherboard) device).setSataPort(Integer.valueOf(fieldValue));
				break;
			case PSI_EXPRESS_SLOT:
				((Motherboard) device).setPsiExpressSolt(Integer.valueOf(fieldValue));
				break;
			case FORM_FACTOR:
				((Motherboard) device).setFormFactor(FormFactorType.fromString(fieldValue));
				break;
			case CPU_SOCKET:
				((Motherboard) device).setCpuSocket(CpuSocketType.fromString(fieldValue));
				break;
			}
		}

		if (device instanceof Videocard) {

			value = true;

			switch (FieldEnum.fromString(fieldName)) {
			case AIR_COOLING:
				((Videocard) device).setAirCooling(Boolean.valueOf(fieldValue));
				break;
			case DVI_PORT:
				((Videocard) device).setDviPort(Integer.valueOf(fieldValue));
				break;
			case HDMI_PORT:
				((Videocard) device).setHdmiPort(Integer.valueOf(fieldValue));
				break;
			case MINI_HDMI_PORT:
				((Videocard) device).setMiniHdmiPort(Boolean.valueOf(fieldValue));
				break;
			case DISPLAY_PORT:
				((Videocard) device).setDisplayPort(Boolean.valueOf(fieldValue));
				break;
			case MINI_DISPLAY_PORT:
				((Videocard) device).setDisplayPort(Boolean.valueOf(fieldValue));
				break;
			case VGA_PORT:
				((Videocard) device).setVgaPort(Boolean.valueOf(fieldValue));
				break;
			case GPU_CLOCK:
				((Videocard) device).setGpuClock(Integer.valueOf(fieldValue));
				break;
			case MEMORY_CLOCK:
				((Videocard) device).setMemoryClock(Integer.valueOf(fieldValue));
				break;

			}

		}
		
		if (value == false){
			LOGGER.log(Level.ERROR, "Object " + fieldName + " is not exist.");
			
		}
		return value;

	}
}
