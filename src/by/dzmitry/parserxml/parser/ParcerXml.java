package by.dzmitry.parserxml.parser;

import java.util.ArrayList;

import by.dzmitry.parserxml.exception.ParserXmlException;

/**
 * The Interface ParcerXml.
 */
public interface ParcerXml {

	/**
	 * Creates the.
	 *
	 * @param path
	 *            the path
	 * @param service
	 *            the service
	 * @return the array list
	 * @throws ParserXmlException 
	 */
	 ArrayList<ParsedItem> create(String path, ParserBuilder service) throws ParserXmlException;
}
