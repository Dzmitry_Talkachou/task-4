package by.dzmitry.parserxml.entity;

/**
 * The Class Videocard.
 */
public class Videocard extends Device {

	/** The airCooling. */
	private boolean airCooling;

	/** The dvi port. */
	private int dviPort;

	/** The hdmi port. */
	private int hdmiPort;

	/** The mini hdmi port. */
	private boolean miniHdmiPort;

	/** The display port. */
	private boolean displayPort;

	/** The mini display port. */
	private boolean miniDisplayPort;

	/** The vga port. */
	private boolean vgaPort;

	/** The gpu clock. */
	private int gpuClock;

	/** The memory clock. */
	private int memoryClock;

	/**
	 * Checks if is air�ooling.
	 *
	 * @return true, if is air�ooling
	 */
	public boolean isAirCooling() {
		return airCooling;
	}

	/**
	 * Sets the airCooling.
	 *
	 * @param airCooling
	 *            the new airCooling
	 */
	public void setAirCooling(boolean airCooling) {
		this.airCooling = airCooling;
	}

	/**
	 * Gets the dvi port.
	 *
	 * @return the dvi port
	 */
	public int getDviPort() {
		return dviPort;
	}

	/**
	 * Sets the dvi port.
	 *
	 * @param dviPort
	 *            the new dvi port
	 */
	public void setDviPort(int dviPort) {
		this.dviPort = dviPort;
	}

	/**
	 * Gets the hdmi port.
	 *
	 * @return the hdmi port
	 */
	public int getHdmiPort() {
		return hdmiPort;
	}

	/**
	 * Sets the hdmi port.
	 *
	 * @param hdmiPort
	 *            the new hdmi port
	 */
	public void setHdmiPort(int hdmiPort) {
		this.hdmiPort = hdmiPort;
	}

	/**
	 * Checks if is mini hdmi port.
	 *
	 * @return true, if is mini hdmi port
	 */
	public boolean isMiniHdmiPort() {
		return miniHdmiPort;
	}

	/**
	 * Sets the mini hdmi port.
	 *
	 * @param miniHdmiPort
	 *            the new mini hdmi port
	 */
	public void setMiniHdmiPort(boolean miniHdmiPort) {
		this.miniHdmiPort = miniHdmiPort;
	}

	/**
	 * Checks if is display port.
	 *
	 * @return true, if is display port
	 */
	public boolean isDisplayPort() {
		return displayPort;
	}

	/**
	 * Sets the display port.
	 *
	 * @param displayPort
	 *            the new display port
	 */
	public void setDisplayPort(boolean displayPort) {
		this.displayPort = displayPort;
	}

	/**
	 * Checks if is mini display port.
	 *
	 * @return true, if is mini display port
	 */
	public boolean isMiniDisplayPort() {
		return miniDisplayPort;
	}

	/**
	 * Sets the mini display port.
	 *
	 * @param miniDisplayPort
	 *            the new mini display port
	 */
	public void setMiniDisplayPort(boolean miniDisplayPort) {
		this.miniDisplayPort = miniDisplayPort;
	}

	/**
	 * Checks if is vga port.
	 *
	 * @return true, if is vga port
	 */
	public boolean isVgaPort() {
		return vgaPort;
	}

	/**
	 * Sets the vga port.
	 *
	 * @param vgaPort
	 *            the new vga port
	 */
	public void setVgaPort(boolean vgaPort) {
		this.vgaPort = vgaPort;
	}

	/**
	 * Gets the gpu clock.
	 *
	 * @return the gpu clock
	 */
	public int getGpuClock() {
		return gpuClock;
	}

	/**
	 * Sets the gpu clock.
	 *
	 * @param gpuClock
	 *            the new gpu clock
	 */
	public void setGpuClock(int gpuClock) {
		this.gpuClock = gpuClock;
	}

	/**
	 * Gets the memory clock.
	 *
	 * @return the memory clock
	 */
	public int getMemoryClock() {
		return memoryClock;
	}

	/**
	 * Sets the memory clock.
	 *
	 * @param memoryClock
	 *            the new memory clock
	 */
	public void setMemoryClock(int memoryClock) {
		this.memoryClock = memoryClock;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.dzmitry.parser.entity.Device#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (airCooling ? 1231 : 1237);
		result = prime * result + (displayPort ? 1231 : 1237);
		result = prime * result + dviPort;
		result = prime * result + gpuClock;
		result = prime * result + hdmiPort;
		result = prime * result + memoryClock;
		result = prime * result + (miniDisplayPort ? 1231 : 1237);
		result = prime * result + (miniHdmiPort ? 1231 : 1237);
		result = prime * result + (vgaPort ? 1231 : 1237);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.dzmitry.parser.entity.Device#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Videocard other = (Videocard) obj;
		if (airCooling != other.airCooling)
			return false;
		if (displayPort != other.displayPort)
			return false;
		if (dviPort != other.dviPort)
			return false;
		if (gpuClock != other.gpuClock)
			return false;
		if (hdmiPort != other.hdmiPort)
			return false;
		if (memoryClock != other.memoryClock)
			return false;
		if (miniDisplayPort != other.miniDisplayPort)
			return false;
		if (miniHdmiPort != other.miniHdmiPort)
			return false;
		if (vgaPort != other.vgaPort)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.dzmitry.parser.entity.Device#toString()
	 */
	@Override
	public String toString() {
		return "Videocard: id=" + getDeviceId() + ", name=" + getName() + ", origin=" + getOrigin() + ", brand="
				+ getBrand() + ", price=" + getPrice() + ", critical=" + isCritical() + ", airCooling=" + airCooling
				+ ", dviPort=" + dviPort + ", hdmiPort=" + hdmiPort + ", miniHdmiPort=" + miniHdmiPort
				+ ", displayPort=" + displayPort + ", miniDisplayPort=" + miniDisplayPort + ", vgaPort=" + vgaPort
				+ ", gpuClock=" + gpuClock + ", memoryClock=" + memoryClock + "\n";
	}
}