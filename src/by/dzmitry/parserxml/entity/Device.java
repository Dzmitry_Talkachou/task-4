package by.dzmitry.parserxml.entity;

import by.dzmitry.parserxml.enumeration.BrandType;
import by.dzmitry.parserxml.enumeration.OriginType;
import by.dzmitry.parserxml.parser.ParsedItem;

/**
 * The Class Device.
 */
public abstract class Device implements ParsedItem {

	/** The device id. */
	private String deviceId;

	/** The name. */
	private String name;

	/** The origin. */
	private OriginType origin;

	/** The brand. */
	private BrandType brand;

	/** The price. */
	private int price;

	/** The critical. */
	private boolean critical;

	/**
	 * Gets the device id.
	 *
	 * @return the device id
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * Sets the device id.
	 *
	 * @param deviceId
	 *            the new device id
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the origin.
	 *
	 * @return the origin
	 */
	public OriginType getOrigin() {
		return origin;
	}

	/**
	 * Sets the origin.
	 *
	 * @param origin
	 *            the new origin
	 */
	public void setOrigin(OriginType origin) {
		this.origin = origin;
	}

	/**
	 * Gets the brand.
	 *
	 * @return the brand
	 */
	public BrandType getBrand() {
		return brand;
	}

	/**
	 * Sets the brand.
	 *
	 * @param brand
	 *            the new brand
	 */
	public void setBrand(BrandType brand) {
		this.brand = brand;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price
	 *            the new price
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * Checks if is critical.
	 *
	 * @return true, if is critical
	 */
	public boolean isCritical() {
		return critical;
	}

	/**
	 * Sets the critical.
	 *
	 * @param critical
	 *            the new critical
	 */
	public void setCritical(boolean critical) {
		this.critical = critical;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brand == null) ? 0 : brand.hashCode());
		result = prime * result + (critical ? 1231 : 1237);
		result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + price;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Device other = (Device) obj;
		if (brand != other.brand)
			return false;
		if (critical != other.critical)
			return false;
		if (deviceId == null) {
			if (other.deviceId != null)
				return false;
		} else if (!deviceId.equals(other.deviceId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (origin != other.origin)
			return false;
		if (price != other.price)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Device [deviceId=" + deviceId + ", name=" + name + ", origin=" + origin + ", brand=" + brand
				+ ", price=" + price + ", critical=" + critical + "]";
	}

}
