package by.dzmitry.parserxml.entity;

import by.dzmitry.parserxml.enumeration.CpuSocketType;
import by.dzmitry.parserxml.enumeration.FormFactorType;

/**
 * The Class Motherboard.
 */
public class Motherboard extends Device {

	/** The com port. */
	private boolean comPort;

	/** The lpt port. */
	private boolean lptPort;

	/** The usb port. */
	private int usbPort;

	/** The sata port. */
	private int sataPort;

	/** The psi express slot. */
	private int psiExpressSolt;

	/** The form factor. */
	private FormFactorType formFactor;

	/** The cpu socket. */
	private CpuSocketType cpuSocket;

	/**
	 * Checks if is com port.
	 *
	 * @return true, if is com port
	 */
	public boolean isComPort() {
		return comPort;
	}

	/**
	 * Sets the com port.
	 *
	 * @param comPort
	 *            the new com port
	 */
	public void setComPort(boolean comPort) {
		this.comPort = comPort;
	}

	/**
	 * Checks if is lpt port.
	 *
	 * @return true, if is lpt port
	 */
	public boolean isLptPort() {
		return lptPort;
	}

	/**
	 * Sets the lpt port.
	 *
	 * @param lptPort
	 *            the new lpt port
	 */
	public void setLptPort(boolean lptPort) {
		this.lptPort = lptPort;
	}

	/**
	 * Gets the usb port.
	 *
	 * @return the usb port
	 */
	public int getUsbPort() {
		return usbPort;
	}

	/**
	 * Sets the usb port.
	 *
	 * @param usbPort
	 *            the new usb port
	 */
	public void setUsbPort(int usbPort) {
		this.usbPort = usbPort;
	}

	/**
	 * Gets the sata port.
	 *
	 * @return the sata port
	 */
	public int getSataPort() {
		return sataPort;
	}

	/**
	 * Sets the sata port.
	 *
	 * @param sataPort
	 *            the new sata port
	 */
	public void setSataPort(int sataPort) {
		this.sataPort = sataPort;
	}

	/**
	 * Gets the psi express solt.
	 *
	 * @return the psi express solt
	 */
	public int getPsiExpressSolt() {
		return psiExpressSolt;
	}

	/**
	 * Sets the psi express solt.
	 *
	 * @param psiExpressSolt
	 *            the new psi express solt
	 */
	public void setPsiExpressSolt(int psiExpressSolt) {
		this.psiExpressSolt = psiExpressSolt;
	}

	/**
	 * Gets the form factor.
	 *
	 * @return the form factor
	 */
	public FormFactorType getFormFactor() {
		return formFactor;
	}

	/**
	 * Sets the form factor.
	 *
	 * @param formFactor
	 *            the new form factor
	 */
	public void setFormFactor(FormFactorType formFactor) {
		this.formFactor = formFactor;
	}

	/**
	 * Gets the cpu socket.
	 *
	 * @return the cpu socket
	 */
	public CpuSocketType getCpuSocket() {
		return cpuSocket;
	}

	/**
	 * Sets the cpu socket.
	 *
	 * @param socket
	 *            the new cpu socket
	 */
	public void setCpuSocket(CpuSocketType socket) {
		this.cpuSocket = socket;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.dzmitry.parser.entity.Device#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (comPort ? 1231 : 1237);
		result = prime * result + ((formFactor == null) ? 0 : formFactor.hashCode());
		result = prime * result + (lptPort ? 1231 : 1237);
		result = prime * result + psiExpressSolt;
		result = prime * result + sataPort;
		result = prime * result + ((cpuSocket == null) ? 0 : cpuSocket.hashCode());
		result = prime * result + usbPort;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.dzmitry.parser.entity.Device#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Motherboard other = (Motherboard) obj;
		if (comPort != other.comPort)
			return false;
		if (formFactor != other.formFactor)
			return false;
		if (lptPort != other.lptPort)
			return false;
		if (psiExpressSolt != other.psiExpressSolt)
			return false;
		if (sataPort != other.sataPort)
			return false;
		if (cpuSocket != other.cpuSocket)
			return false;
		if (usbPort != other.usbPort)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.dzmitry.parser.entity.Device#toString()
	 */
	@Override
	public String toString() {
		return "Motherboard: id=" + getDeviceId() + ", name=" + getName() + ", origin=" + getOrigin() + ", brand="
				+ getBrand() + ", price=" + getPrice() + ", critical=" + isCritical() + ", comPort=" + comPort
				+ ", lptPort=" + lptPort + ", usbPort=" + usbPort + ", sataPort=" + sataPort + ", psiExpressSolt="
				+ psiExpressSolt + ", formFactor=" + formFactor + ", socket=" + cpuSocket + "\n";
	}
}