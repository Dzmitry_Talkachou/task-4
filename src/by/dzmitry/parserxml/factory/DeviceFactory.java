package by.dzmitry.parserxml.factory;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.dzmitry.parserxml.entity.Device;
import by.dzmitry.parserxml.entity.Motherboard;
import by.dzmitry.parserxml.entity.Videocard;
import by.dzmitry.parserxml.enumeration.DeviceType;

/**
 * A factory for creating Device objects.
 */
public class DeviceFactory {

	private static final Logger LOGGER = LogManager.getLogger(DeviceFactory.class.getName());

	/**
	 * Creates the.
	 *
	 * @param type
	 *            the type
	 * @return the device
	 */
	public Device create(DeviceType type) {

		Device device = null;

		switch (type) {

		case MOTHERBOARD:
			device = new Motherboard();
			break;

		case VIDEOCARD:
			device = new Videocard();
			break;

		default:
			LOGGER.log(Level.ERROR, "Object " + type + " is not created.");
		}
		return device;
	}
}