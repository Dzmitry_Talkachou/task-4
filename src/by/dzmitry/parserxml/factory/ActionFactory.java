package by.dzmitry.parserxml.factory;

import javax.servlet.http.HttpServletRequest;

import by.dzmitry.parserxml.command.ActionCommand;
import by.dzmitry.parserxml.command.EmptyCommand;
import by.dzmitry.parserxml.enumeration.CommandEnum;
import by.dzmitry.parserxml.manager.MessageManager;

public class ActionFactory {
	
	public ActionCommand defineCommand(HttpServletRequest request) {
		
		ActionCommand current = new EmptyCommand();
		
		String action = request.getParameter("command");
		
		if (action == null || action.isEmpty()) {
		
			return current;
		}
		try {
			CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
			
			current = currentEnum.getCurrentCommand();
			
		} catch (IllegalArgumentException e) {
			
			request.setAttribute("wrongAction", action + MessageManager.getProperty("message.wrongaction"));
		}
		return current;
	}
}