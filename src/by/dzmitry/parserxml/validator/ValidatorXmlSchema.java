package by.dzmitry.parserxml.validator;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import by.dzmitry.parserxml.exception.ParserXmlException;

/**
 * The Class ValidatorXMLSchema.
 */
public class ValidatorXmlSchema {

	/**
	 * Validate.
	 *
	 * @param xsdPath
	 *            the xsd path
	 * @param xmlPath
	 *            the xml path
	 * @throws ParserXmlException
	 */
	public void validate(String xsdPath, String xmlPath) throws ParserXmlException {

		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema;
		try {
			schema = factory.newSchema(new File(xsdPath));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new File(xmlPath)));
			
		} catch (SAXException | IOException e) {

			throw new ParserXmlException("Exception in ValidatorXMLSchema class " + e);

		}
	}
}