package by.dzmitry.parserxml.exception;

/**
 * The Class ParserException.
 */
public class ParserXmlException extends Exception {

	private static final long serialVersionUID = -1102944275630398310L;

	/**
	 * Instantiates a new parser exception.
	 */
	public ParserXmlException() {
		super();

	}

	/**
	 * Instantiates a new parser exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 * @param enableSuppression
	 *            the enable suppression
	 * @param writableStackTrace
	 *            the writable stack trace
	 */
	public ParserXmlException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	
	}

	/**
	 * Instantiates a new parser exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public ParserXmlException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * Instantiates a new parser exception.
	 *
	 * @param message
	 *            the message
	 */
	public ParserXmlException(String message) {
		super(message);

	}

	/**
	 * Instantiates a new parser exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public ParserXmlException(Throwable cause) {
		super(cause);
	
	}
}
