package by.dzmitry.parserxml.logic;

import java.util.ArrayList;

import by.dzmitry.parserxml.enumeration.DeviceType;
import by.dzmitry.parserxml.exception.ParserXmlException;
import by.dzmitry.parserxml.manager.ConfigurationManager;
import by.dzmitry.parserxml.parser.DeviceBuilder;
import by.dzmitry.parserxml.parser.ParcerXml;
import by.dzmitry.parserxml.parser.ParsedItem;
import by.dzmitry.parserxml.parser.ParserBuilder;
import by.dzmitry.parserxml.validator.ValidatorXmlSchema;

public class ParserLogic {

	public static ArrayList<ParsedItem> parse(String path, ParcerXml parser, String pageSelectorValue)
			throws ParserXmlException {

		ParserBuilder service = new DeviceBuilder().add(DeviceType.fromString(pageSelectorValue));

		ValidatorXmlSchema validator = new ValidatorXmlSchema();

		validator.validate(path + ConfigurationManager.getProperty("path.parser.xsd"),
				path + ConfigurationManager.getProperty("path.parser.xml"));
		
		ArrayList<ParsedItem> parsedItems = parser.create(path + ConfigurationManager.getProperty("path.parser.xml"),
				service);


		return parsedItems;
	}
}