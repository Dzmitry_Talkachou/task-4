<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link rel="icon" type="image/x-icon" href="images/favicon.ico">
<link
	href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,400italic|Montserrat|Roboto+Slab:400,700|Bitter:400,700,400italic'
	rel='stylesheet' type='text/css'>
<script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
<script src="js/jquery.cookie.js" type="text/javascript"></script>
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<title>Computer parts store.</title>
</head>
<body>
	<div class="deviceTypeSelector">
		<div>COOKIES</div>
		<input type="radio" name="deviceType" value="videocard">Videocard<br />
		<input type="radio" name="deviceType" value="motherboard">Motherboard<br />
	</div>
	<div class="deviceTypeSelectorInfo"  style="display: none">
	text
</div>
	<div class="parserSelector">
		<div>AJAX</div>
		<input type="radio" name="parserType" value="sax" ${sax}>ParserSax<br />
		<input type="radio" name="parserType" value="stax" ${stax}>ParseStax<br />
		<input type="radio" name="parserType" value="dom" ${dom}>ParserDom<br />
	</div>
	<div class="parserSelectorInfo"  style="display: none">
	text
</div>
	<div class="productList">
		<div id="ajaxGetResponse"></div>
	</div>
	<div class="backButtonDiv" style="display: none">
		<input type="button" name="backButton" value="Back">
	</div>
</body>
</html>