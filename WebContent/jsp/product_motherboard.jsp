<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="productList">
	<c:forEach var="elem" items="${lst}" varStatus="status">
		<div class="productCard">
			<div class="productName">
				<c:out value="${ elem.name }" />
			</div>
			<div class="productId">
				Id:
				<c:out value="${ elem.deviceId }" />
			</div>
			<div class="productImage">
				<img src="images/${ elem.deviceId }.jpg">
			</div>
			<div class="productPrice number">
				<c:out value="${ elem.price }" />  
				<span class="moneyName">BYR</span>
			</div>
			<div class="adittionalInformation" style="display: none">

				<div class="additionalInfo">
					Critical:<img src="images/<c:out value="${ elem.critical }" />.gif">
				</div>
				<div class="productBrand">
					Com port:<img src="images/<c:out value="${ elem.comPort }" />.gif">
				</div>
				<div class="additionalInfo">
					LPT port:<img src="images/<c:out value="${ elem.lptPort }" />.gif">
				</div>
				<div class="additionalInfo">
					USB ports:
					<c:out value="${ elem.usbPort }" />
				</div>
				<div class="additionalInfo">
					SATA ports:
					<c:out value="${ elem.sataPort }" />
				</div>
				<div class="additionalInfo">
					PSIexpress slots:
					<c:out value="${ elem.psiExpressSolt }" />
				</div>
				<div class="additionalInfo">
					Form-factor:
					<c:out value="${ elem.formFactor }" />
				</div>
				<div class="additionalInfo">
					CPU Socket type:
					<c:out value="${ elem.cpuSocket }" />
				</div>
			</div>

		</div>
	</c:forEach>
</div>