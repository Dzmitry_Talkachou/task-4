<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="productList">
	<c:forEach var="elem" items="${lst}" varStatus="status">
		<div class="productCard">
			<div class="productName">
				<c:out value="${ elem.name }" />
			</div>
			<div class="productId">
				Id:
				<c:out value="${ elem.deviceId }" />
			</div>
			<div class="productImage">
				<img src="images/${ elem.deviceId }.jpg">
			</div>
			<div class="productPrice number">
				<c:out value="${ elem.price }" />
				<span class="moneyName">BYR</span>
			</div>
			<div class="adittionalInformation" style="display: none">
				<div class="productBrand">
					Brand:
					<c:out value="${ elem.brand }" />
				</div>
				<div class="additionalInfo">
					Critical:<img src="images/<c:out value="${ elem.critical }" />.gif">
				</div>
				<div class="additionalInfo">
					Air cooling:<img
						src="images/<c:out value="${ elem.airCooling }" />.gif">
				</div>
				<div class="additionalInfo">
					DVI ports:
					<c:out value="${ elem.dviPort }" />
				</div>
				<div class="additionalInfo">
					HDMI port:
					<c:out value="${ elem.hdmiPort }" />
				</div>
				<div class="additionalInfo">
					Vga port:<img src="images/<c:out value="${ elem.vgaPort }" />.gif">
				</div>
				<div class="additionalInfo">
					Display port:<img
						src="images/<c:out value="${ elem.displayPort }" />.gif">
				</div>
				<div class="additionalInfo">
					Mini displayPort:<img
						src="images/<c:out value="${ elem.miniDisplayPort }" />.gif">
				</div>
				<div class="additionalInfo">
					GPU clock:
					<c:out value="${ elem.gpuClock }" />
				</div>
				<div class="additionalInfo">
					Memory clock:
					<c:out value="${ elem.memoryClock }" />
				</div>
			</div>

		</div>
	</c:forEach>
</div>