$(document).ready(
        function() {

          // change Device type
          $("input[name=deviceType]").click(
                  function() {
                    $.cookie('page_selector', $(
                            "input[name='deviceType']:checked").val());
                    $('.deviceTypeSelectorInfo').text(
                            "Cookie 'page_selector'='"
                                    + $("input[name='deviceType']:checked")
                                            .val() + "' saved ");
                    $('.deviceTypeSelectorInfo').show();
                    $('.deviceTypeSelectorInfo').delay(3000).fadeOut();

                    // imitate click on Parser type if Parser type checked
                    if ($("input[name='parserType']:checked").val()) {
                      $("input[name='parserType']:checked").trigger('click');
                    }

                  });

          $('.productCard').mouseenter(function() {

            $(this).width(230);

          });

          if (!$.cookie('page_selector')) {

            $.cookie('page_selector', 'Videocard');

          }
          ;

          // if page_selector cookie exist, then check Device type
          if ($.cookie('page_selector')) {
            $(
                    "input[name=deviceType][value='"
                            + $.cookie('page_selector') + "']").attr('checked',
                    'checked');
          }

          // ajax request
          $("input[name='parserType']").click(
                  function() {

                    $.ajax({
                      url: 'controller',
                      data: {
                        command: 'parser',
                        parserType: $("input[name='parserType']:checked").val()
                      },
                      success: function(responseText) {
                        $('#ajaxGetResponse').html(
                                '<div class="typeParserMessage">Parser '
                                        + $("input[name='parserType']:checked")
                                                .val().toUpperCase() + '</div>'
                                        + responseText);

                        $('.parserSelectorInfo').text(
                                'Ajax request success. Parser '
                                        + $("input[name='parserType']:checked")
                                                .val().toUpperCase());
                        $('.parserSelectorInfo').show();
                        $('.parserSelectorInfo').delay(3000).fadeOut();

                        // radio-button check and set default value
                        if (!$("input[name='parserType']:checked").val()) {
                          $("input[name=parserType][value='sax']").attr(
                                  'checked', 'checked');
                        }

                        // product page
                        $(".productCard").click(function() {
                          name = $(this).text();

                          $('.productCard').not($(this)).hide();

                          $(this).height(230);
                          $('.parserSelectorInfo').hide();
                          $('.deviceTypeSelectorInfo').hide();
                          $('.adittionalInformation').show();
                          $(".deviceTypeSelector").hide();
                          $(".parserSelector").hide();
                          $(".typeParserMessage").hide();
                          document.title = name;
                          $(".backButtonDiv").show();

                        });

                        // back to index page
                        $("input[name=backButton]").click(function() {
                          document.title = "Computer parts store.";
                          $('.productCard').height(200);
                          $('.productCard').show();
                          $(".backButtonDiv").hide();
                          $(".deviceTypeSelector").show();
                          $(".parserSelector").show();
                          $(".typeParserMessage").show();
                          $('.adittionalInformation').hide();
                        });

                        // number format 1,000,000
                        $('.number').text(function() {
                          var str = $(this).html() + '';
                          x = str.split('.');
                          x1 = x[0];
                          x2 = x.length > 1 ? '.' + x[1] : '';
                          var rgx = /(\d+)(\d{3})/;
                          while (rgx.test(x1)) {
                            x1 = x1.replace(rgx, '$1' + ',' + '$2');
                          }
                          $(this).html(x1 + x2);
                        });

                      }
                    });
                  });
        });
